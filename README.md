ansible-ravendb
=========

[RavenDB](https://ravendb.net/)
![ravendb-logo](https://ravendb.net/img/raven_logo.svg "RavenDB")

A self-contained Ansible role to assist in ~~all~~ most of your RavenDB 4.x+ needs! Supports testing via [molecule](https://www.digitalocean.com/community/tutorials/how-to-implement-continuous-testing-of-ansible-roles-using-molecule-and-travis-ci-on-ubuntu-18-04). This role is kept intentionally "simpler", and more flat for newer readers. Given this, PRs will not be accepted to break out playbooks, use higher level conventions, etc.

Enjoy!

Quick Start
------------

execution:

Check out the [Example Playbook](#example-playbook) section.

testing:

```
git clone git@gitlab.com:adrift/ansible-ravendb.git

cd ansible

molecule test
```

Requirements
------------

Docker

Role Variables
--------------

```yaml
ravendb_version:           "4.2.2"
ravendb_install_path:      "/opt"

ravendb_package_checksum:  "sha256:1FAB5D78126D5C4A374AA42D0F775E50039F822CF6CB0064D569CC826C8E5DD2"
ravendb_package_path:      "{{ ansible_user_dir }}/RavenDB-{{ ravendb_version }}-linux-x64.tar.bz2"

ravendb_server_url:        "{{ ansible_default_ipv4.address }}"
ravendb_public_server_url: "{{ ansible_default_ipv4.address }}"
ravendb_port:              "8080"
ravendb_user:              "{{ ansible_user }}"

ravendb_setup_mode:        "Initial"
ravendb_monitoring:        false
ravendb_snmp_password:     "ravendb"
```

Dependencies
------------

Pip:

- docker

A list of other roles hosted on Galaxy should go here, plus any details in
regards to parameters that may need to be set for other roles, or variables that
are used from other roles.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables
passed in as parameters) is always nice for users too:

```yaml
    - hosts: servers
      roles:
         - { role: ansible-ravendb, ravendb_server_url: a.myravennode.domain.com, ravendb_public_server_url: a.myravennode.domain.com }
```

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a
website (HTML is not allowed).
